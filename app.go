package main

import (
	"net/http"
	"github.com/gin-gonic/gin"
	"talktalk-backend/posts"
	"talktalk-backend/comments"
	"talktalk-backend/users"
)

func main() {

	r := gin.Default()

	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"status": "ok"})
	})

	r.POST("/reg", users.ProcessReg)

	r.POST("/posts", posts.ProcessPosts)

	r.POST("/addpost", posts.ProcessAddPost)

	r.POST("/comments/:postId", comments.ProcessComments)

	r.POST("/addcomment", comments.ProcessAddComment)

	r.Run(":3000")
}