package model

import "time"

func GetMockupPosts() []Post {
	var posts []Post



	p1 := Post{
		PostId:"post_id_1",
		Lat:55.6374996,
		Lon:37.597901,
		Message:"Ох уж эти пробки!",
		TypeId:TALK_TYPE_PURPLE,
		Date:time.Now().Add(15 * time.Minute)}

	posts = append(posts,p1)

	p2 := Post{
		PostId:"post_id_2",
		Lat:55.592751240675604,
		Lon:37.59843269557483,
		Message:"Есть у кого зарядка для айфона?",
		TypeId:TALK_TYPE_GREEN,
		Date:time.Now().Add(120 * time.Minute)}

	posts = append(posts,p2)

	p3 := Post{
		PostId:"post_id_9",
		Lat:55.6376921,
		Lon:37.5975911,
		Message:"Куплю машину ближе к перекрестку",
		TypeId:TALK_TYPE_RED,
		Date:time.Now()}

	posts = append(posts,p3)

	return posts
}
