package model

import "time"

const TALK_TYPE_PURPLE = "conversationPurple"
const TALK_TYPE_GREEN = "conversationGreen"
const TALK_TYPE_YELLOW = "conversationYellow"
const TALK_TYPE_BLUE = "conversationBlue"
const TALK_TYPE_RED = "conversationRed"

const TALK_TYPE_ACCIDENT = "accident"
const TALK_TYPE_ROADWORK = "roadWork"
const TALK_TYPE_CAMERA = "camera"
const TALK_TYPE_OTHER = "other"



type Post struct {
	PostId string `json:"postId" bson:"postId"`
	UserId string `json:"-" bson:"userId"`
	Lat float64 `json:"latitude" bson:"lat"`
	Lon float64 `json:"longitude" bson:"lon"`
	Message string `json:"message" bson:"message"`
	TypeId string `json:"typeId" bson:"typeId"`
	Date time.Time `json:"date" bson:"date"`
	FacebookId string `json:"facebookId" bson:"facebookId"`
	DisplayName string `json:"displayName" bson:"displayName"`
}
