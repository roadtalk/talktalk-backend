package model

import (
	"time"
	"github.com/rs/xid"
	"talktalk-backend/db"
	"gopkg.in/mgo.v2/bson"
	"talktalk-backend/posts/distance"
)

func CreatePost(postFromReq Post) (Post, error) {
	newPost := Post{
		PostId 		: xid.New().String(),
		UserId		: postFromReq.UserId,
		Date    	: time.Now().UTC(),
		Message		: postFromReq.Message,
		Lat		: postFromReq.Lat,
		Lon		: postFromReq.Lon,
		TypeId		: postFromReq.TypeId,
		FacebookId	: postFromReq.FacebookId,
		DisplayName	: postFromReq.DisplayName,
	}

	session := db.GetSession()
	defer session.Close()

	err := db.GetPosts(session).Insert(newPost)

	return newPost, err
}

func FindPostByReqData(postFromReq Post) (Post, error) {
	session := db.GetSession()
	defer session.Close()

	query := bson.M{"userId" : postFromReq.UserId, "message" : postFromReq.Message, "typeId" : postFromReq.TypeId, "lat" : postFromReq.Lat, "lon" : postFromReq.Lon}

	var p Post
	err := db.GetPosts(session).Find(query).One(&p)

	return p, err
}

func GetAllPosts() ([]Post, error) {
	session := db.GetSession()
	defer session.Close()

	var ps []Post

	err := db.GetPosts(session).Find(bson.M{}).All(&ps)

	return ps, err
}

func GetPostsByRadius(bounds distance.PostsBounds) ([]Post, error) {
	session := db.GetSession()
	defer session.Close()

	var ps []Post

	query := bson.M{"$and": []interface{}{
		bson.M{"lat" : bson.M{"$gte": bounds.MinLat}},
		bson.M{"lat" : bson.M{"$lte": bounds.MaxLat}},
		bson.M{	"lon" : bson.M{"$gte": bounds.MinLon}},
		bson.M{	"lon" : bson.M{"$lte": bounds.MaxLon}},
	}}

	err := db.GetPosts(session).Find(query).All(&ps)

	return ps, err
}