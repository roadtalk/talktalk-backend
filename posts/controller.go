package posts

import (
	"github.com/gin-gonic/gin"
	"net/http"
	postmodel "talktalk-backend/posts/model"
	usermodel "talktalk-backend/users/model"
	"gopkg.in/go-playground/validator.v9"
	"fmt"
	"gopkg.in/mgo.v2"
	"unicode/utf8"
	"talktalk-backend/config"
	"talktalk-backend/posts/distance"
)

var validate *validator.Validate

type PostsReqData struct {
	UserId string `json:"userId" validate:"required"`
	Radius int `json:"radius" validate:"required"`
	Lat float64 `json:"latitude" validate:"required"`
	Lon float64 `json:"longitude" validate:"required"`
}

type AddPostReqData struct {
	UserId string `json:"userId" validate:"required"`
	Lat float64 `json:"latitude" validate:"required"`
	Lon float64 `json:"longitude" validate:"required"`
	Message string `json:"message" validate:"required"`
	TypeId string `json:"typeId" validate:"required"`
}

func ProcessPosts(c *gin.Context) {
	var json PostsReqData

	err := c.BindJSON(&json)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "json validation error"})
		return
	}

	validate = validator.New()

	errValid := validate.Struct(json)

	if errValid != nil {
		fmt.Println("add comment validation err: ",errValid)
		c.JSON(http.StatusBadRequest, gin.H{"error": "object validation error"})
		return
	}

	_, errUser := usermodel.FindUserByUserId(json.UserId)
	if errUser == mgo.ErrNotFound {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "bad user id"})
		return
	}

	if errUser != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": config.SHRUG})
		return
	}

	if json.Radius == 0 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "bad value for raduis field"})
		return
	}

	bounds := distance.DetermineBounds(json.Lat, json.Lon, json.Radius)

	ps, err := postmodel.GetPostsByRadius(bounds)

	if err == mgo.ErrNotFound || len(ps) == 0{
		empty := make([]string, 0)
		c.JSON(http.StatusOK,empty)
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": config.SHRUG})
		return
	}

	c.JSON(http.StatusOK, ps)
	return
}

func ProcessAddPost(c *gin.Context) {
	var json AddPostReqData

	err := c.BindJSON(&json)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "json validation error"})
		return
	}

	validate = validator.New()

	errValid := validate.Struct(json)

	if errValid != nil {
		fmt.Println("err: ",errValid)
		c.JSON(http.StatusBadRequest, gin.H{"error": "object validation error"})
		return
	}

	u, errUser := usermodel.FindUserByUserId(json.UserId)
	if errUser == mgo.ErrNotFound {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "bad user id"})
		return
	}

	if errUser != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": config.SHRUG})
		return
	}

	msgLen := utf8.RuneCountInString(json.Message)

	if msgLen < config.POST_MIN_LEN || msgLen > config.POST_MAX_LEN {
		c.JSON(http.StatusBadRequest, gin.H{"error": "message length error"})
		return
	}

	postFromReq := postmodel.Post{
		UserId		: json.UserId,
		Lat		: json.Lat,
		Lon		: json.Lon,
		Message		: json.Message,
		TypeId		: json.TypeId,
		FacebookId	: u.FacebookId,
		DisplayName	: u.DisplayName,
		}


	oldPost, err := postmodel.FindPostByReqData(postFromReq)

	if err != mgo.ErrNotFound {
		c.JSON(http.StatusOK, gin.H{"postId": oldPost.PostId})
		return
	}

	newPost, err := postmodel.CreatePost(postFromReq)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": config.SHRUG})
		return
	}

	if newPost.PostId == "" {
		c.JSON(http.StatusInternalServerError, gin.H{"error": config.SHRUG})
		return
	}

	c.JSON(http.StatusOK, gin.H{"postId": newPost.PostId})
	return
}



