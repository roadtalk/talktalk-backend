package distance

import (
	"github.com/alouche/go-geolib"
	"math"
)


type PostsBounds struct {
	MinLon float64
	MaxLon float64
	MinLat float64
	MaxLat float64
}

func DetermineBounds(lat float64, lon float64, radInt int) PostsBounds {

	if radInt < 1000 {
		radInt = 1000
	}

	radius := float64(radInt*1) / 1000.0

	minLat := lat - geolib.Rad2Deg(radius/geolib.GreatEarthCircleRadiusKM)
	maxLat := lat + geolib.Rad2Deg(radius/geolib.GreatEarthCircleRadiusKM)

	minLon := lon - geolib.Rad2Deg(math.Asin(radius/geolib.GreatEarthCircleRadiusKM)) / math.Cos(geolib.Deg2Rad(lat))
	maxLon := lon + geolib.Rad2Deg(math.Asin(radius/geolib.GreatEarthCircleRadiusKM)) / math.Cos(geolib.Deg2Rad(lat))

	return PostsBounds{MinLat: minLat, MaxLat: maxLat, MinLon: minLon, MaxLon : maxLon}
}
