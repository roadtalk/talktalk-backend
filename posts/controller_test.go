package posts

import (
	"testing"
	"talktalk-backend/posts/distance"
	"talktalk-backend/posts/model"
	"fmt"
)

func TestProcessPosts(t *testing.T) {

	bounds := distance.DetermineBounds(55.592751240675604, 37.59843269557483, 3000)

	ps, err := model.GetPostsByRadius(bounds)

	fmt.Println("ps:",ps)

	fmt.Println("err: ",err)
}
