package config

const SHRUG = `¯\_(ツ)_/¯`

const POST_MIN_LEN = 3
const POST_MAX_LEN = 200

const COMMENT_MIN_LEN = 3
const COMMENT_MAX_LEN = 200
