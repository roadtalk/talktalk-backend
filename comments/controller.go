package comments

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"fmt"
	commentmodel "talktalk-backend/comments/model"
	usermodel "talktalk-backend/users/model"
	"gopkg.in/go-playground/validator.v9"
	"unicode/utf8"
	"gopkg.in/mgo.v2"
	"talktalk-backend/config"
)

type AddCommentReqData struct {
	UserId string `json:"userId" validate:"required"`
	PostId string `json:"postId" validate:"required"`
	Message string `json:"message" validate:"required"`
}

var validate *validator.Validate

func ProcessComments(c * gin.Context) {
	postId := c.Param("postId")

	cmnts, err := commentmodel.GetCommentsByPostId(postId)

	if err == mgo.ErrNotFound || len(cmnts) == 0{
		empty := make([]string, 0)
		c.JSON(http.StatusOK,empty)
		return
	}

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": config.SHRUG})
		return
	}

	c.JSON(http.StatusOK, cmnts)
	return
}

func ProcessAddComment(c *gin.Context) {
	var json AddCommentReqData

	err := c.BindJSON(&json)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "json validation error"})
		return
	}

	validate = validator.New()

	errValid := validate.Struct(json)

	if errValid != nil {
		fmt.Println("add comment validation err: ",errValid)
		c.JSON(http.StatusBadRequest, gin.H{"error": "object validation error"})
		return
	}

	u, errUser := usermodel.FindUserByUserId(json.UserId)
	if errUser == mgo.ErrNotFound {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "bad user id"})
		return
	}

	if errUser != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": config.SHRUG})
		return
	}

	msgLen := utf8.RuneCountInString(json.Message)

	if msgLen < config.COMMENT_MIN_LEN || msgLen > config.COMMENT_MAX_LEN {
		c.JSON(http.StatusBadRequest, gin.H{"error": "message length error"})
		return
	}

	commentFromReq := commentmodel.Comment{
		UserId		: json.UserId,
		PostId		: json.PostId,
		Message		: json.Message,
		FacebookId	: u.FacebookId,
		DisplayName	: u.DisplayName,
	}

	oldComment, err := commentmodel.FindCommentByReqData(commentFromReq)

	if err != mgo.ErrNotFound {
		c.JSON(http.StatusOK, gin.H{"commentId": oldComment.CommentId})
		return
	}

	newComment, err := commentmodel.CreateComment(commentFromReq)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": config.SHRUG})
		return
	}

	if newComment.CommentId == "" {
		c.JSON(http.StatusInternalServerError, gin.H{"error": config.SHRUG})
		return
	}

	c.JSON(http.StatusOK, gin.H{"commentId": newComment.CommentId})
	return
}
