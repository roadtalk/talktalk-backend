package model

import "time"

type Comment struct {
	PostId string `json:"-" bson:"postId"`
	UserId string `json:"-" bson:"userId"`
	CommentId string `json:"commentId" bson:"commentId"`
	Message string `json:"message" bson:"message"`
	FacebookId string `json:"facebookId" bson:"facebookId"`
	DisplayName string `json:"displayName" bson:"displayName"`
	Date time.Time `json:"date" bson:"date"`
}
