package model

import "time"

func GetMockupComments() []Comment {
	var comments []Comment

	c1 := Comment{
		PostId:"post_id_1",
		Message:"Давно стоишь?",
		FacebookId:"test1",
		CommentId:"asdsadfsdgf",
		Date:time.Now().Add(15 * time.Minute)}

	comments = append(comments,c1)

	c2 := Comment{
		PostId:"post_id_2",
		Message:"Познакомлюсь с красивой девушкой😏",
		FacebookId:"test2335",
		CommentId:"zosdwer",
		Date:time.Now().Add(120 * time.Minute)}

	comments = append(comments,c2)

	c3 := Comment{
		PostId:"post_id_9",
		Message:"+++",
		FacebookId:"test78248",
		CommentId:"oomomsdasdqw",
		Date:time.Now()}

	comments = append(comments,c3)

	return comments
}
