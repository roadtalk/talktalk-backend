package model

import (
	"time"
	"gopkg.in/mgo.v2/bson"
	"talktalk-backend/db"
	"github.com/rs/xid"
)

func GetCommentsByPostId(postId string) ([]Comment, error) {
	session := db.GetSession()
	defer session.Close()

	query := bson.M{"postId" : postId}

	var cmnts []Comment
	err := db.GetComments(session).Find(query).All(&cmnts)

	return cmnts, err
}

func FindCommentByReqData(commentFromReq Comment) (Comment, error) {
	session := db.GetSession()
	defer session.Close()

	query := bson.M{"userId" : commentFromReq.UserId, "message" : commentFromReq.Message, "postId" : commentFromReq.PostId}

	var cmnt Comment
	err := db.GetComments(session).Find(query).One(&cmnt)

	return cmnt, err
}

func CreateComment(commentFromReq Comment) (Comment, error) {
	newComment := Comment{
		PostId		: commentFromReq.PostId,
		UserId		: commentFromReq.UserId,
		FacebookId	: commentFromReq.FacebookId,
		DisplayName	: commentFromReq.DisplayName,
		Message		: commentFromReq.Message,
		CommentId	: xid.New().String(),
		Date		: time.Now().UTC(),
	}

	session := db.GetSession()
	defer session.Close()

	err := db.GetComments(session).Insert(newComment)

	return newComment, err
}
