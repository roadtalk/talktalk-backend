package users

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"unicode/utf8"
	"talktalk-backend/users/model"
	"talktalk-backend/config"
	"gopkg.in/go-playground/validator.v9"
)


type RegReqData struct {
	FacebookId string `json:"facebookId" validate:"required"`
	DisplayName string `json:"displayName" validate:"required"`
}

var validate *validator.Validate

func ProcessReg(c * gin.Context) {
	var json RegReqData

	err := c.BindJSON(&json)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "json validation error"})
		return
	}

	validate = validator.New()

	errValid := validate.Struct(json)

	if errValid != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "object validation error"})
		return
	}

	fbIdLen := utf8.RuneCountInString(json.FacebookId)
	nameLen := utf8.RuneCountInString(json.DisplayName)

	if fbIdLen <= 2 || fbIdLen >= 100 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "bad facebook id"})
		return
	}

	if nameLen <= 2 || nameLen >= 100 {
		c.JSON(http.StatusBadRequest, gin.H{"error": "bad display name"})
		return
	}

	oldUser, err := model.FindUserByFacebookId(json.FacebookId)

	if err == nil && oldUser.UserId != "" {
		c.JSON(http.StatusOK, gin.H{"userId": oldUser.UserId})
		return
	}

	newUser, dbErr := model.CreateUser(json.FacebookId, json.DisplayName)

	if dbErr != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": config.SHRUG})
		return
	}

	if newUser.UserId == "" {
		c.JSON(http.StatusInternalServerError, gin.H{"error": config.SHRUG})
		return
	}


	c.JSON(http.StatusOK, gin.H{"userId": newUser.UserId})
	return
}
