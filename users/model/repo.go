package model

import (
	"github.com/rs/xid"
	"time"
	"talktalk-backend/db"
	"gopkg.in/mgo.v2/bson"
)

func CreateUser(facebookId string, displayName string) (User, error) {
	newUser := User{
		FacebookId : facebookId,
		UserId     : xid.New().String(),
		DisplayName: displayName,
		RegDate    : time.Now().UTC(),
	}

	session := db.GetSession()
	defer session.Close()

	err := db.GetUsers(session).Insert(newUser)

	return newUser, err
}

func FindUserByFacebookId(facebookId string) (User, error) {
	session := db.GetSession()
	defer session.Close()

	var u User
	query := bson.M{"facebookId" : facebookId}
	err := db.GetUsers(session).Find(query).One(&u)

	return u, err
}

func FindUserByUserId(userId string) (User, error) {
	session := db.GetSession()
	defer session.Close()

	var u User
	query := bson.M{"userId" : userId}
	err := db.GetUsers(session).Find(query).One(&u)

	return u, err
}


