package model

import "time"

type User struct {
	UserId string `json:"-" bson:"userId"`
	DisplayName string `json:"-" bson:"displayName"`
	FacebookId string `json:"-" bson:"facebookId"`
	RegDate time.Time `json:"-" bson:"regDate"`
}