package db

import (
	"gopkg.in/mgo.v2"
	"time"
)

var GlobalSession *mgo.Session

const MONGO_URL = "mongodb://localhost"

func init() {
	session, err := mgo.Dial(MONGO_URL)
	if err != nil {
		time.Sleep(50 * time.Millisecond)
		session, err = mgo.Dial(MONGO_URL)
		if err != nil {
			panic(err)
		}
	}
	session.SetMode(mgo.Monotonic, true)
	GlobalSession = session
}
