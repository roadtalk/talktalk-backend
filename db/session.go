package db

import (
	"gopkg.in/mgo.v2"
)

const DB_NAME = "talktalk"

func GetSession() *mgo.Session {
	return GlobalSession.New()
}

func GetUsers(session *mgo.Session) *mgo.Collection {
	return session.DB(DB_NAME).C("users")
}

func GetPosts(session *mgo.Session) *mgo.Collection {
	return session.DB(DB_NAME).C("posts")
}

func GetComments(session *mgo.Session) *mgo.Collection {
	return session.DB(DB_NAME).C("comments")
}
